#include <fstream>
#include <string>
#include <stdlib.h>
#include <unistd.h>
#include <curl/curl.h>
#include <fstream>

//#define DEBUG

#define RED std::string("\033[0;31m")
#define BOLDRED std::string("\033[1;31m")
#define GREEN std::string("\033[0;32m")
#define BOLDGREEN std::string("\033[1;32m")
#define YELLOW std::string("\033[0;33m")
#define BOLDYELLOW std::string("\033[1;33m")
#define BLUE std::string("\033[0;34m")
#define BOLDBLUE std::string("\033[1;34m")
#define MAGENTA std::string("\033[0;35m")
#define BOLDMAGENTA std::string("\033[1;35m")
#define CYAN std::string("\033[0;36m")
#define BOLDCYAN std::string("\033[1;36m")
#define RESET std::string("\033[0m")

bool debug = false;

size_t write_data(void *buffer, size_t size, size_t nmemb, void *userp)
{
   return size * nmemb;
}

void dgprint (std::string txt) {
    if (debug){
        printf("%s", txt.c_str());
    }
}

int main(int argc, char* argv[]) {
    long jmp = 1;
    long jmpa = 0;
    long loadedstatus = 0;
    std::string file = "emoteIDs.txt";
    std::string sfile;
    bool savestat = false;

    std::string lfile;
    bool loadstat = false;

    bool ow = false;

    long response_code;
    CURL *curl;
    CURLcode res;
    curl_global_init(CURL_GLOBAL_DEFAULT);


    std::string temp = BOLDGREEN + "\nloading Discord" + BOLDCYAN +"Emote" + BOLDGREEN + "Scanner\n" + BOLDRED + "by: " + BOLDGREEN + "LeMoonStar\n\n" + RESET;
    printf("%s",temp.c_str());
    if (argc > 2) {
        long bn = atol(argv[1]);
        long en = atol(argv[2]);
        if (bn > en) {
            printf ("\033[1;31m<beginn number> needs to be lower than <end number>\033[0m\n");
            curl_global_cleanup();
            return(1);
        }

            dgprint("handle extra args started!\n");

        if (argc > 3) {
            std::string temp2;
            for (int i = 3;i<=argc-1;i++) {

                    dgprint("entering for loop\n");

                temp2 = argv[i];
                if (temp2 == "-j") {
                    i++;
                    if (i<argc) {
                        jmp = atol(argv[i]);
                    }else{
                        printf("\033[1;31mmissing argument!\033[0m\n");
                        return 0;
                    }
                }else if(temp2 == "-ja") {
                    i++;
                    if (i<argc) {
                        jmpa = atol(argv[i]) - 1;
                    }else{
                        printf("\033[1;31mmissing argument!\033[0m\n");
                        return 0;
                    }
                }else if(temp2 == "-f") {
                    i++;
                    if (i<argc) {
                        file = argv[i];
                    }else{
                        printf("\033[1;31mmissing argument!\033[0m\n");
                        return 0;
                    }
                }else if (temp2 == "-debug"){
                    debug = true;
                }else if (temp2 == "-save"){
                    i++;
                    if (i<argc) {
                        savestat = true;
                        sfile = argv[i];
                    }else{
                        printf("\033[1;31mmissing argument!\033[0m\n");
                        return 0;
                    }
                }else if (temp2 == "-load"){
                    i++;
                    if (i<argc) {
                        loadstat = true;
                        lfile = argv[i];
                    }else{
                        printf("\033[1;31mmissing argument!\033[0m\n");
                        return 0;
                    }
                }else{
                    printf("unknown option: %s !",argv[i]);
                    return(0);
                }
            }

                //dgprint("jmp = %ld\njmpa = %ld\n",jmp,jmpa);

        }
        if (loadstat) {
            std::ifstream loadfile (lfile.c_str(), std::ifstream::binary | std::ifstream::in);
            loadfile >> jmp >> jmpa >> file >> ow >> bn >> en >> loadedstatus;
            loadfile.close();
            savestat = true;
        }
        if (savestat){
            std::ofstream savefile (sfile.c_str(), std::ofstream::binary | std::ofstream::out);
            savefile << jmp << std::endl << jmpa << std::endl << file << std::endl << ow << std::endl << bn << std::endl << en << std::endl << bn;
            savefile.close();
        }

            dgprint("handle extra args finished!\n");

        //std::string temp = bn + std::string("to") + en;
        //printf("%s",temp.c_str());
        long range = (en - bn) / jmp;
        printf("\033[0;32mscanning %ld ID`s :emoteID:%ld to emoteID:%ld\033[0m\n",range,bn,en);
        curl = curl_easy_init();
        if (!curl){
            curl_global_cleanup();
            return(1);
        }

        //std::ofstream out ("emoteIDs.txt", std::ofstream::out);

            dgprint("starting fstream\n");

        std::ofstream out (file.c_str(), std::ios::app);
        out << "\n\nNewScan:\n";
        out.close();

            dgprint("closing fstream\n");

        for (long i=bn+loadedstatus;i<=en;i+=jmp) {
            std::string url = "https://discordapp.com/api/v6/emojis/" + std::to_string(i) + ".png";
            printf("\033[0;32mtesting:\033[0;34m%s\033[0m  %ld/%ld\n",url.c_str(),((i-bn) / jmp),range);
            curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
            curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, true);
            curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
            res = curl_easy_perform(curl);
            if(res != CURLE_OK) {
                fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
                response_code = 200;
            }
            curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);
            if (response_code == 200){
                printf("\033[0;33mfound!\033[0;34m %ld\033[0;32m is a emoteID!\033[0m\n",i);

                    dgprint("starting fstream\n");

                std::ofstream out (file.c_str(), std::ios::app); //newmethod
                out << i << "\n";
                out.close(); //newmethod

                    dgprint("closing fstream\n");

                i +=jmpa;
            }
            if (savestat){
                std::ofstream savefile (sfile.c_str(), std::ofstream::binary | std::ofstream::out);
                savefile << jmp << std::endl << jmpa << std::endl << file << std::endl << ow << std::endl << bn << std::endl << en << std::endl << (i-bn);
                savefile.close();
            }
        }
        //out.close();
        curl_easy_cleanup(curl);
    }else{
        temp = BOLDRED + "syntax:\n" + BOLDYELLOW + "des " + GREEN + "<beginn number> <end number>\n" + RESET;
        printf("%s",temp.c_str());
    }
    /*if (curl) {
        curl_esay_cleanup();
    }*/
    curl_global_cleanup();
    return(0);
}
